<?php

    class Realise
    {
        private ?Album $album;
        private ?Artiste $artiste;
        private int $nb_morceau;

        public function __construct($album, $artiste, $nb_morceau)
        {  
            $this->album = $album;
            $this->artiste = $artiste;
            $this->nb_morceau = $nb_morceau;
        }

        public function getAlbum()
        {
            return $this->album;
        }

        public function getArtiste()
        {
            return $this->artiste;
        }

        public function getNbMorceau()
        {
            return $this->nb_morceau;
        }
    }

?>