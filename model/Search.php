<?php

    class Search
    {
        private ?Album $albums;
        private ?array $artistes;

        public function __construct($album, $artistes)
        {  
            $this->albums = $album;
            $this->artistes = $artistes;
        }

        public function getAlbum()
        {
            return $this->albums;
        }

        public function getArtistes()
        {
            return $this->artistes;
        }
    }

?>