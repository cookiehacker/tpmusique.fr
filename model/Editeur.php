<?php
    class Editeur
    {
        private int $id_editeur;
        private string $nom_editeur;

        public function __construct($id_editeur ,$nom_editeur)
        {
            $this->id_editeur = $id_editeur;
            $this->nom_editeur = $nom_editeur;
        }

        public function getIdEditeur()
        {
            return $this->id_editeur;
        }

        public function getNomEditeur()
        {
            return $this->nom_editeur;
        }
        
    }
?>