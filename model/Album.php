<?php

    class Album
    {
        private int $id_album;
        private string $nom;
        private string $nom_genre;
        private int $annee_album;
        private int $id_editeur;

        public function __construct(
            $id_album,
            $nom,
            $nom_genre,
            $annee_album,
            $id_editeur
        )
        {
            $this->id_album = $id_album;
            $this->nom = $nom;
            $this->nom_genre = $nom_genre;
            $this->annee_album = $annee_album;
            $this->id_editeur = $id_editeur;
        }

        public function getIdAlbum()
        {
            return $this->id_album;
        }

        public function getNomAlbum()
        {
            return $this->nom;
        }

        public function getNomGenre()
        {
            return $this->nom_genre;
        }

        public function getAnneeAlbum()
        {
            return $this->annee_album;
        }

        public function getIdEditeur()
        {
            return $this->id_editeur;
        }
    }

?>