<?php
    class Comporte
    {
        private ?Morceaux $morceaux;
        private ?Album $album;

        public function __construct($morceaux, $album)
        {
            $this->morceaux = $morceaux;
            $this->album = $album;
        }

        public function getMorceaux()
        {
            return $this->morceaux;
        }

        public function getAlbum()
        {
            return $this->album;
        }
    }

?>