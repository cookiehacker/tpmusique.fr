<?php

    class ArtisteByAlbum
    {
        private int $id_artiste;
        private string $nom_artiste;
        private string $prenom_artiste;
        private int $nb_morceau;
        private int $id_album;
        private string $nom_album;

        public function __construct(
            $id_artiste,
            $nom_artiste,
            $prenom_artiste,
            $nb_morceau,
            $id_album,
            $nom_album
        )
        {
            $this->id_artiste = $id_artiste;
            $this->nom_artiste = $nom_artiste;
            $this->prenom_artiste = $prenom_artiste;
            $this->nb_morceau = $nb_morceau;
            $this->id_album = $id_album;
            $this->nom_album = $nom_album;
        }

        public function getIdArtiste()
        {
            return $this->id_artiste;
        }

        public function getNomArtiste()
        {
            return $this->nom_artiste;
        }

        public function getPrenomArtiste()
        {
            return $this->prenom_artiste;
        }

        public function getNbMorceau()
        {
            return $this->nb_morceau;
        }

        public function getIdAlbum()
        {
            return $this->id_album;
        }

        public function getNomAlbum()
        {
            return $this->nom_album;
        }
    }