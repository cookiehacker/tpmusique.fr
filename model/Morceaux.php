<?php

    class Morceaux
    {
        private int $id_morceau;
        private string $titre_morceau;
        private int $duree_morceau;
        private $nom_fichier;
        private $type_fichier;

        public function __construct(
            $id_morceau,
            $titre_morceau,
            $duree_morceau,
            $nom_fichier,
            $type_fichier
        )
        {
            $this->id_morceau = $id_morceau;
            $this->titre_morceau = $titre_morceau;
            $this->duree_morceau = $duree_morceau;
            $this->nom_fichier = $nom_fichier;
            $this->type_fichier = $type_fichier;
        }

        public function getIdMorceau()
        {
            return $this->id_morceau;
        }

        public function getTitreMorceau()
        {
            return $this->titre_morceau;
        }

        public function getDureeMorceau()
        {
            return $this->duree_morceau;
        }

        public function getNomFichier()
        {
            return $this->nom_fichier;
        }

        public function getTypeFichier()
        {
            return $this->type_fichier;
        }
    }