<?php
    class Artiste
    {
        private int $id_artiste;
        private string $nom_artiste;
        private string $prenom_artiste;

        public function __construct($id_artiste ,$nom_artiste, $prenom_artiste)
        {
            $this->id_artiste = $id_artiste;
            $this->nom_artiste = $nom_artiste;
            $this->prenom_artiste = $prenom_artiste;
        }

        public function getIdArtiste()
        {
            return $this->id_artiste;
        }

        public function getNomArtiste()
        {
            return $this->nom_artiste;
        }

        public function getPrenomArtiste()
        {
            return $this->prenom_artiste;
        }

        
    }
?>