
<link type="text/css" rel="stylesheet" href="static/css/bootstrap.min.css" media="screen,projection" />

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="/home">TP - BD</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor02">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link active" href="/home">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/album">Albums</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/recherche">Rechercher</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/ajoutArtiste">Ajout d'un artiste</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/ajoutAlbum">Ajout d'un album</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/lieralbumartiste">Lier Album avec Artiste</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="/suppAlbum">Supprimer un album</a>
        </li>
      </ul>

      <form class="d-flex" action="/recherche" method="GET">
        <input name="fastsearch" class="form-control me-sm-2" type="text" placeholder="Recherche rapide d'un album">
        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Rechercher</button>
      </form>
    </div>
  </div>
</nav>

<script type="text/javascript" src="static/js/jquery.min.js"></script>
<script type="text/javascript" src="static/js/bootstrap.bundle.min.js"></script>
