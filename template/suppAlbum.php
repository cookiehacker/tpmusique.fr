<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement(); 
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/formSuppAlbum.php";
    ?>
    <?php
        $album = $db->getListAlbum();
        if (is_null($album))
        {
            echo "<h3>Pas de données trouvées !</h3>";
        }
        else 
        {
    ?>
    <table class='table table-hover'>
        <thead>
            <tr>
                <th scope="col">Titre de l'album</th>
                <th scope="col">Genre</th>
                <th scope="col">Année</th>
                <th scope="col">Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
                foreach($album as $data)
                {
                    echo "<tr class='table-dark'>";
                    echo "  <td>".$data->getNomAlbum()."</td>";
                    echo "  <td>".$data->getNomGenre()."</td>";
                    echo "  <td>".$data->getAnneeAlbum()."</td>";
                    echo "<td>";
                    echo "  <form method='POST'>";
                    echo "      <button name='delete' value='".$data->getIdAlbum()."' type='submit' class='btn btn-danger'>Delete</button>";
                    echo "  </form>";
                    echo "</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
    <?php
    }   
    ?>
</body>
</html>