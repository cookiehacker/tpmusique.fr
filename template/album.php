<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement();  
    ?>
    <form method="POST">
        <div class="form-group">
            <label for="selectAlbum" class="form-label mt-4">Choix de l'album</label>
            <select name="id_album" class="form-select" id="selectAlbum">
                <?php
                    $albums = $db->getListAlbum();
                    if (is_null($albums))
                    {
                        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Aucun album trouvé</div>";
                    }
                    else
                    {
                        foreach($albums as $album)
                        {
                            echo "<option value=".$album->getIdAlbum().">".$album->getNomAlbum()."</option>";
                        }
                    }
                ?>
            </select>
        </div>
        <center><button name='submit' type="submit" class="btn btn-primary">Valider</button></center>
    </form>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/choixAlbum.php";
    ?>
</body>
</html>