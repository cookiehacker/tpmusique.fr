<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>

    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement(); 
    ?>

    <form method="POST">
    <fieldset>
        <div class="row">
            <div class="col">
                <label for="nomAlbum" class="form-label mt-4">Nom de l'album</label>
                <input name="nomAlbum" type="text" class="form-control" id="nomAlbum" placeholder="Nom de l'album" required>
            </div>
            <div class="col">
                <label for="artiste" class="form-label mt-4">Nom de l'artiste</label>
                <select name="artiste" class="form-select" id="artiste">
                    <?php
                        $artistes = $db->getListArtiste();
                        foreach($artistes as &$value)
                        {
                            echo "<option value=".$value->getIdArtiste()." >".$value->getNomArtiste()." -- ".$value->getPrenomArtiste()."</option>";
                        }
                    ?>
                </select>
            </div>
        </div>
    </fieldset>
        <br/>
        <center>
            <button name="submit" type="submit" class="btn btn-primary">Rechercher</button>
        </center>
    </form>

    <br />
    
    <center>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/rechercheAlbum.php";
    ?>
    </center>
</body>
</html>