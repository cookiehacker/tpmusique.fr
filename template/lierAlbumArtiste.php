<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement();  
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/formAjoutRealise.php";
    ?>

    <form method="POST">
        <div class="row">
            <div class="col">
                <label for="album" class="form-label mt-4">Album</label>
                <select name="album" class="form-select" id="album">
                    <?php
                        $albums = $db->getListAlbum();
                        foreach($albums as &$value)
                        {
                            echo "<option value=".$value->getIdAlbum()." >".$value->getNomAlbum()."</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="col">
                <label for="artiste" class="form-label mt-4">Artiste</label>
                <select name="artiste" class="form-select" id="artiste">
                    <?php
                        $artistes = $db->getListArtiste();
                        foreach($artistes as &$value)
                        {
                            echo "<option value=".$value->getIdArtiste()." >".$value->getNomArtiste()." -- ".$value->getPrenomArtiste()."</option>\n";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="nbmorceau">Nombre de morceaux</label>
                <input name="nbmorceau" type="number" class="form-control" id="nbmorceau" placeholder="Nombre de morceaux" required>
            </div>
        </div>
        </br>
        <center>
            <button name="submit" type="submit" class="btn btn-primary">Ajouter</button>
        </center>
    </form>
</body>
</html>