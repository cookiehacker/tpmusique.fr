<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";

        if(isset($_GET['statut']))
        {
            if($_GET['statut'] == "success")
            {
                echo "<div class='alert alert-dismissible alert-success'>";
                echo "  <button type='button' class='btn-close' data-bs-dismiss='alert'></button>";
                echo "  <strong>Bravo !</strong> L'utilisateur a été ajouté !";
                echo "</div>";
            }
            else if($_GET['statut'] == "error")
            {
                echo "<div class='alert alert-dismissible alert-danger'>";
                echo "  <button type='button' class='btn-close' data-bs-dismiss='alert'></button>";
                echo "  <strong>Erreur !</strong> L'utilisateur n'a pas été ajouté !";
                echo "</div>";
            }
        }
    ?>

    <h1>tpMusique.fr : le meilleur site de musique ever !</h1>

    <h2>Nos artistes !</h2>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";

        $db = new dbmanagement();
        $artistes = $db->getListArtiste();

        if (is_null($artistes))
        {
            echo "<h3>Pas d'artistes trouvés !</h3>";
        }
        else
        {
            echo "<ul>";
            foreach($artistes as $a)
            {
                echo "<li>".$a->getNomArtiste()." - ".$a->getPrenomArtiste()."</li>";
            }
            echo "</ul>";
        }
    ?>
    <h2>Nos artistes par album !</h2>
    <?php
        $artist_by_album = $db->getListArtisteByAlbum();
        if (is_null($artist_by_album))
        {
            echo "<h3>Pas de données trouvées !</h3>";
        }
        else 
        {
    ?>

        <table class='table table-hover'>
            <thead>
                <tr>
                    <th scope="col">Titre de l'album</th>
                    <th scope="col">Artiste</th>
                    <th scope="col">Nombre morceaux</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    foreach($artist_by_album as $data)
                    {
                        echo "<tr class='table-dark'>";
                        echo "  <td>".$data->getNomAlbum()."</td>";
                        echo "  <td>".$data->getPrenomArtiste()." ".$data->getNomArtiste()."</td>";
                        echo "  <td>".$data->getNbMorceau()."</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    <?php
        }   
    ?>
</body>

<!--
     ----- (  ()
   ' o      (   ()
  '     o    ( ()
  |  o     o | 
   '   o  o ' 
     ------  

Hackers gonna hack
Cookies gonna control the world
-->

</html>