<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement();  
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/formAjoutAlbum.php";
    ?>

    <form method="POST">
        <div class="row">
            <div class="col">
                <label for="nomAlbum">Nom de l'album</label>
                <input name="nomAlbum" type="text" class="form-control" id="nomAlbum" placeholder="Nom de l'album" required>
            </div>
            <div class="col">
                <label for="nomGenre">Nom du genre</label>
                <input name="nomGenre" type="text" class="form-control" id="nomGenre" placeholder="Nom du genre" required>
            </div>
            <div class="col">
                <label for="anneeAlbum">Année de l'album</label>
                <input name="anneeAlbum" type="number" class="form-control" id="anneeAlbum" placeholder="Année de l'album" required>
            </div>
            <div class="col">
                <label for="editeur">Editeur</label>
                <select name="editeur" class="form-select" id="editeur">
                    <?php
                        $editeurs = $db->getListEditeur();
                        foreach($editeurs as &$value)
                        {
                            echo "<option value=".$value->getIdEditeur()." >".$value->getNomEditeur()."</option>\n";
                        }
                    ?>
                </select>
            </div>
        </div>
        </br>
        <center>
            <button name="submit" type="submit" class="btn btn-primary">Ajouter</button>
        </center>
    </form>
</body>
</html>