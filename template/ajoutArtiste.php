<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tpMusique.fr</title>
</head>
<body>
    <?php
        require_once $_SERVER['DOCUMENT_ROOT']."/template/menu.php";
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/dbmanagement.php";
        $db = new dbmanagement();  
        require_once $_SERVER['DOCUMENT_ROOT']."/controller/formAjoutArtiste.php";
    ?>

    <form method="POST">
        <div class="row">
            <div class="col">
                <label for="nomArtiste">Nom de l'artiste</label>
                <input name="nomArtiste" type="text" class="form-control" id="nomArtiste" placeholder="Nom de l'artiste" required>
            </div>
            <div class="col">
                <label for="prenomArtiste">Prenom de l'artiste</label>
                <input name="prenomArtiste" type="text" class="form-control" id="prenomArtiste" placeholder="Prenom de l'artiste" required>
            </div>
        </div>
        </br>
        <center>
            <button name="submit" type="submit" class="btn btn-primary">Ajouter</button>
        </center>
    </form>
</body>
</html>