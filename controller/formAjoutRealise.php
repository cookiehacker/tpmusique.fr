<?php

if (
    (
        isset($_POST['album']) && 
        isset($_POST['artiste']) && 
        isset($_POST['nbmorceau']) && 
        isset($_POST['submit'])
    ) 
)
{
    if (
        empty($_POST['album']) || 
        empty($_POST['artiste']) ||
        empty($_POST['nbmorceau']))
    {
        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Le nom, le genre et l'année de l'album sont nécessaire !</div>";
    }
    else
    {
        if (!ctype_digit($_POST['album']))
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'album doit être numérique !</div>";
        }
        else if (!ctype_digit($_POST['artiste']))
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'artiste doit être numérique !</div>";
        }
        else if (!ctype_digit($_POST['nbmorceau']))
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Le nombre de morceau doit être numérique !</div>";
        }
        else
        {
            $album      = (int) escapeshellcmd(htmlspecialchars($_POST['album']));
            $artiste    = (int) escapeshellcmd(htmlspecialchars($_POST['artiste']));
            $nbmorceau  = (int) escapeshellcmd(htmlspecialchars($_POST['nbmorceau']));
            

            if($db->ajoutRealise($album,$artiste,$nbmorceau))
            {
                echo "<div class='alert alert-success'><strong>Bravo ! </strong>La liason est ajouté</div>";
            }
            else
            {
                echo "<div class='alert alert-danger'><strong>Erreur ! </strong>La liason n'est pas ajouté</div>";
            }
        }
    }
}

?>