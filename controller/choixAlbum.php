<?php


if ((isset($_POST['submit']) && isset($_POST['id_album'])) && filter_var($_POST['id_album'], FILTER_VALIDATE_INT)) {
    $id_album = escapeshellcmd(htmlspecialchars($_POST['id_album'])); # I'm not paranoid... Just a little...
    require_once $_SERVER['DOCUMENT_ROOT'] . "/controller/dbmanagement.php";

    $results = $db->getListMorceauByAlbum($id_album);
    if (is_null($results)) {
        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Pas de données trouvées pour l'album id : " . $id_album . "</div>";
    } else {
?>
        <h2>Album : <?php echo $results[0]->getAlbum()->getNomAlbum() ?></h2>
        <table class='table table-hover'>
            <thead>
                <tr>
                    <th scope="col">ID du morceaux</th>
                    <th scope="col">Titre du morceau</th>
                    <th scope="col">Duree du morceau (s)</th>
                </tr>

            <tbody>
                <?php
                foreach ($results as $data) {
                    echo "<tr class='table-dark'>";
                    echo "  <td>" . $data->getMorceaux()->getIdMorceau() . "</td>";
                    echo "  <td>" . $data->getMorceaux()->getTitreMorceau() . "</td>";
                    echo "  <td>" . $data->getMorceaux()->getDureeMorceau() . "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
            </thead>
        </table>
<?php
    }
}
?>