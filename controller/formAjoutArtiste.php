<?php

if (
    (
        isset($_POST['prenomArtiste']) && 
        isset($_POST['nomArtiste']) && 
        isset($_POST['submit'])
    ) 
)
{
    if (empty($_POST['prenomArtiste']) || empty($_POST['nomArtiste']))
    {
        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Le nom et prénom de l'artiste sont nécessaire !</div>";
    }
    else
    {
        $nomartiste     = escapeshellcmd(htmlspecialchars($_POST['nomArtiste']));
        $prenomartiste  = escapeshellcmd(htmlspecialchars($_POST['prenomArtiste']));

        if($db->ajoutArtiste(
            new Artiste(0,$nomartiste,$prenomartiste)
        ))
        {
            echo "<div class='alert alert-success'><strong>Bravo ! </strong>L'artiste est ajouté</div>";
        }
        else
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'artiste n'est pas ajouté</div>";
        }
    }
}

?>