<?php


require_once($_SERVER['DOCUMENT_ROOT']."/model/Artiste.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/ArtisteByAlbum.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Comporte.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Morceaux.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Album.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Realise.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Search.php");
require_once($_SERVER['DOCUMENT_ROOT']."/model/Editeur.php");

class dbmanagement
{
    private $dc;

    public function __construct()
    {
        $this->dc = include $_SERVER['DOCUMENT_ROOT'] . "/config/databaseConfig.php";
    }

    private function connexionDB()
    {
        try
        {
            $conn = new PDO(
                "mysql:
                    host=".$this->dc['HOST'].".;
                    dbname=".$this->dc['DATABASE'].";
                    charset=utf8",
                $this->dc['USER'],
                $this->dc['PASSWORD']
            );
        }
        catch(PDOException $e) {
            die("Impossible de se connecter ! ".$e->getMessage());
        }

        return $conn;
    }

    public function getListArtiste()
    {
        $conn = $this->connexionDB();
        $sql = "SELECT * FROM ARTISTE ORDER BY nom_artiste;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $artistes = array();
            foreach($result as &$value)
            {
                array_push($artistes, new Artiste(
                    $value['id_artiste'],
                    $value['nom_artiste'],
                    $value['prenom_artiste']
                ));
            }
            $stmt->closeCursor();
            return $artistes;
        }
        $stmt->closeCursor();
        return NULL;
    }

    public function getListEditeur()
    {
        $conn = $this->connexionDB();
        $sql = "SELECT * FROM EDITEUR ORDER BY nom_editeur;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $editeurs = array();
            foreach($result as &$value)
            {
                array_push($editeurs, new Editeur(
                    $value['id_editeur'],
                    $value['nom_editeur']
                ));
            }
            $stmt->closeCursor();
            return $editeurs;
        }
        $stmt->closeCursor();
        return NULL;
    }

    public function getListArtisteByAlbum()
    {
        $conn = $this->connexionDB();
        $sql = "SELECT * FROM link_artiste_by_album";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $artist_by_album = array();
            foreach($result as &$value)
            {
                array_push($artist_by_album, new ArtisteByAlbum(
                    $value['id_artiste'],
                    $value['nom_artiste'],
                    $value['prenom_artiste'],
                    $value['nb_morceau'],
                    $value['id_album'],
                    $value['nom']
                ));
            }
            $stmt->closeCursor();
            return $artist_by_album;
        }
        $stmt->closeCursor();
        return NULL;
    }

    public function getListMorceauByAlbum($idalbum)
    {
        $conn = $this->connexionDB();
        $sql = "
            SELECT 
                m.id_morceau, 
                titre_morceau, 
                duree_morceau, 
                nom_fichier, 
                type_fichier, 
                a.id_album, 
                nom, 
                nom_genre, 
                annee_album,
                a.id_editeur
            FROM 
                MORCEAUX m 
                INNER JOIN COMPORTE c 
                INNER JOIN ALBUM a 
            WHERE 
                m.id_morceau = c.id_morceau 
                AND c.id_album = a.id_album 
                AND a.id_album=:i;";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":i", $idalbum, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $morceaux_by_album = array();
            foreach($result as &$value)
            {
                array_push($morceaux_by_album, new Comporte(
                    new Morceaux(
                        $value['id_morceau'],
                        $value['titre_morceau'],
                        $value['duree_morceau'],
                        $value['nom_fichier'],
                        $value['type_fichier']
                    ),
                    new Album(
                        $value['id_album'],
                        $value['nom'],
                        $value['nom_genre'],
                        $value['annee_album'],
                        $value['id_editeur']
                    )
                ));
            }
            $stmt->closeCursor();
            return $morceaux_by_album;
        }
        $stmt->closeCursor();
        return NULL;
    }
    
    function getListAlbum()
    {
        $conn = $this->connexionDB();
        $sql = "SELECT * FROM ALBUM";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $albums = array();
            foreach($result as &$value)
            {
                array_push($albums, new Album(
                    $value['id_album'],
                    $value['nom'],
                    $value['nom_genre'],
                    $value['annee_album'],
                    $value['id_editeur']
                ));
            }
            $stmt->closeCursor();
            return $albums;
        }
        $stmt->closeCursor();
        return NULL;
    }

    function fastSearchAlbum($nom)
    {
        $conn = $this->connexionDB();
        $sql = "
            SELECT 
                a.id_album, 
                nom, 
                nom_genre, 
                annee_album, 
                ar.id_artiste, 
                nom_artiste, 
                prenom_artiste,
                a.id_editeur
            FROM 
                ALBUM a 
                INNER JOIN REALISE r 
                INNER JOIN ARTISTE ar 
            WHERE 
                a.id_album = r.id_album 
                AND r.id_artiste = ar.id_artiste 
                AND nom LIKE :s
            ORDER BY a.id_album;";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":s", $nom.'%', PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $fastsearchs = array();
            $currentAlbum = new Album(
                $result[0]['id_album'],
                $result[0]['nom'],
                $result[0]['nom_genre'],
                $result[0]['annee_album'],
                $result[0]['id_editeur']
            );
            $listArtistes = array();

            foreach($result as &$value)
            {
                if($currentAlbum->getIdAlbum() == $value['id_album'])
                {
                    array_push($listArtistes, new Artiste(
                        $value['id_artiste'],
                        $value['nom_artiste'],
                        $value['prenom_artiste']
                    ));
                }
                else
                {
                    array_push($fastsearchs, new Search(
                        $currentAlbum, $listArtistes
                    ));
                    $currentAlbum = new Album(
                        $value['id_album'],
                        $value['nom'],
                        $value['nom_genre'],
                        $value['annee_album'],
                        $value['id_editeur']
                    );
                }
            }
            array_push($fastsearchs, new Search(
                $currentAlbum, $listArtistes
            ));
            $stmt->closeCursor();
            return $fastsearchs;
        }
        $stmt->closeCursor();
        return NULL;
    }

    function searchAlbum($nom, $artisteID)
    {
        $conn = $this->connexionDB();
        $sql = "
            SELECT 
                a.id_album, 
                nom, 
                nom_genre, 
                annee_album, 
                ar.id_artiste, 
                nom_artiste, 
                prenom_artiste,
                a.id_editeur
            FROM 
                ALBUM a 
                INNER JOIN REALISE r 
                INNER JOIN ARTISTE ar 
            WHERE 
                a.id_album = r.id_album 
                AND r.id_artiste = ar.id_artiste 
                AND nom LIKE :s
                AND ar.id_artiste=:a
            ORDER BY a.id_album;";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":s", $nom.'%', PDO::PARAM_STR);
        $stmt->bindValue(":a", $artisteID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if(!empty($result)) {
            $fastsearchs = array();
            $currentAlbum = new Album(
                $result[0]['id_album'],
                $result[0]['nom'],
                $result[0]['nom_genre'],
                $result[0]['annee_album'],
                $result[0]['id_editeur']
            );
            $listArtistes = array();

            foreach($result as &$value)
            {
                if($currentAlbum->getIdAlbum() == $value['id_album'])
                {
                    array_push($listArtistes, new Artiste(
                        $value['id_artiste'],
                        $value['nom_artiste'],
                        $value['prenom_artiste']
                    ));
                }
                else
                {
                    array_push($fastsearchs, new Search(
                        $currentAlbum, $listArtistes
                    ));
                    $currentAlbum = new Album(
                        $value['id_album'],
                        $value['nom'],
                        $value['nom_genre'],
                        $value['annee_album'],
                        $result['id_editeur']
                    );
                }
            }
            array_push($fastsearchs, new Search(
                $currentAlbum, $listArtistes
            ));
            $stmt->closeCursor();
            return $fastsearchs;
        }
        $stmt->closeCursor();
        return NULL;
    }

    function ajoutArtiste($artiste)
    {
        $conn = $this->connexionDB();
        $sql = "
            INSERT INTO ARTISTE (
                nom_artiste, prenom_artiste
            )
            VALUE (
                :n,:p
            );
        ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":n", $artiste->getNomArtiste(), PDO::PARAM_STR);
        $stmt->bindValue(":p", $artiste->getPrenomArtiste(), PDO::PARAM_STR);
        return $stmt->execute();
    }

    function ajoutAlbum($album)
    {
        $conn = $this->connexionDB();
        $sql = "
            INSERT INTO ALBUM (
                nom, nom_genre, annee_album, id_editeur
            )
            VALUE (
                :n,:g,:a,:e
            );
        ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":n", $album->getNomAlbum(), PDO::PARAM_STR);
        $stmt->bindValue(":g", $album->getNomGenre(), PDO::PARAM_STR);
        $stmt->bindValue(":a", $album->getAnneeAlbum(), PDO::PARAM_INT);
        $stmt->bindValue(":e", $album->getIdEditeur(), PDO::PARAM_INT);
        return $stmt->execute();
    }

    function ajoutRealise($idalbum, $idartiste, $nbmorceaux)
    {
        $conn = $this->connexionDB();
        $sql = "
            INSERT INTO REALISE (
                id_album, id_artiste, nb_morceau
            )
            VALUE (
                :a,:b,:n
            );
        ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":a", $idalbum, PDO::PARAM_INT);
        $stmt->bindValue(":b", $idartiste, PDO::PARAM_INT);
        $stmt->bindValue(":n", $nbmorceaux, PDO::PARAM_INT);
        return $stmt->execute();
    }

    function suppAlbum($idalbum)
    {
        $conn = $this->connexionDB();
        $sql = "
            DELETE FROM REALISE WHERE id_album = :a;
            DELETE FROM ALBUM WHERE id_album = :a;
        ";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":a", $idalbum, PDO::PARAM_INT);
        return $stmt->execute();
    }
}

?>