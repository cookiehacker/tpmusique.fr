<?php

if (
    (
        isset($_POST['delete'])
    ) 
)
{
    if (
        empty($_POST['delete'])
    )
    {
        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Le nom, le genre et l'année de l'album sont nécessaire !</div>";
    }
    else
    {
        if (!ctype_digit($_POST['delete']))
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'album doit être numérique !</div>";
        }
        else
        {
            $album      = (int) escapeshellcmd(htmlspecialchars($_POST['delete']));
            

            if($db->suppAlbum($album))
            {
                echo "<div class='alert alert-success'><strong>Bravo ! </strong>L'album est supprimé</div>";
            }
            else
            {
                echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'album n'est pas supprimé</div>";
            }
        }
    }
}

?>