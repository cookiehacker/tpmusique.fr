<?php
        if (
            (!isset($_POST['artiste']) && !isset($_POST['nomAlbum'])) 
            && isset($_GET['fastsearch'])
        )
        {
            $fs = escapeshellcmd(htmlspecialchars($_GET['fastsearch']));
            
            $results = $db->fastSearchAlbum($fs);
            if (is_null($results))
            {
                echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Aucun album trouvé</div>";
            }
            else
            {
                foreach($results as &$result)
                {
                ?>
                <div class="card text-white bg-secondary mb-3" style="max-width: 20rem;">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo $result->getAlbum()->getNomAlbum(); ?></h4>
                        <form action="/album" method="post">
                            <input hidden name="id_album" value="<?php echo $result->getAlbum()->getIdAlbum();?>" />
                            <center><button name='submit' type="submit" class="btn btn-primary">En savoir plus</button></center>
                        </form>
                    </div>
                </div>
                <?php
                }
            }
        }
        else if((isset($_POST['artiste']) && isset($_POST['nomAlbum']) && isset($_POST['submit'])))
        {
            if(empty($_POST['nomAlbum']))
            {
                echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Nom de l'album vide</div>";
            }
            else
            {
                if (!ctype_digit($_POST['artiste']))
                {
                    echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'ID de l'artiste doit être numérique !</div>";
                }
                else
                {
                    $artisteID  = (int) escapeshellcmd(htmlspecialchars($_POST['artiste']));
                    $nomalbum   = escapeshellcmd(htmlspecialchars($_POST['nomAlbum']));
        
                    $results = $db->searchAlbum($nomalbum, $artisteID);
        
                    if (is_null($results))
                    {
                        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Aucun album trouvé</div>";
                    }
                    else
                    {
                        foreach($results as &$result)
                        {
                        ?>
                        <div class="card text-white bg-secondary mb-3" style="max-width: 20rem;">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $result->getAlbum()->getNomAlbum(); ?></h4>
                                <form action="/album" method="post">
                                    <input hidden name="id_album" value="<?php echo $result->getAlbum()->getIdAlbum();?>" />
                                    <center><button name='submit' type="submit" class="btn btn-primary">En savoir plus</button></center>
                                </form>
                            </div>
                        </div>
                        <?php
                        }
                    }
                }
            }
            

        }
    ?>