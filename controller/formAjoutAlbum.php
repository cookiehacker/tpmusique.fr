<?php

if (
    (
        isset($_POST['nomAlbum']) && 
        isset($_POST['nomGenre']) && 
        isset($_POST['anneeAlbum']) && 
        isset($_POST['editeur']) &&
        isset($_POST['submit'])
    ) 
)
{
    if (
        empty($_POST['nomAlbum']) || 
        empty($_POST['nomGenre']) ||
        empty($_POST['anneeAlbum'])||
        empty($_POST['editeur']))
    {
        echo "<div class='alert alert-danger'><strong>Erreur ! </strong>Le nom, le genre et l'année de l'album sont nécessaire !</div>";
    }
    else
    {
        if (!ctype_digit($_POST['anneeAlbum']))
        {
            echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'année doit être numérique !</div>";
        }
        else
        {
            $nomalbum       = escapeshellcmd(htmlspecialchars($_POST['nomAlbum']));
            $nomgenre       = escapeshellcmd(htmlspecialchars($_POST['nomGenre']));
            $anneealbum     = (int) escapeshellcmd(htmlspecialchars($_POST['anneeAlbum']));
            $idEditeur      = (int) escapeshellcmd(htmlspecialchars($_POST['editeur']));
            

            if($db->ajoutAlbum(
                new Album(0,$nomalbum,$nomgenre,$anneealbum, $idEditeur)
            ))
            {
                echo "<div class='alert alert-success'><strong>Bravo ! </strong>L'album est ajouté</div>";
            }
            else
            {
                echo "<div class='alert alert-danger'><strong>Erreur ! </strong>L'album n'est pas ajouté</div>";
            }
        }
    }
}

?>